FROM php:7.3-fpm-alpine

RUN apk update && \
	apk upgrade && \
    apk --update add \
    supervisor && \
    docker-php-ext-install opcache bcmath pdo_mysql && \
    rm -rf /var/cache/apk/* /tmp/* /var/tmp/*

#composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer > /dev/null

#xdebug
ARG INSTALL_XDEBUG=false
RUN if [ ${INSTALL_XDEBUG} = true ]; then \
    apk --update --no-cache add autoconf g++ make && \
    pecl install -f xdebug && \
    docker-php-ext-enable xdebug && \
    apk del --purge autoconf g++ make \
;fi

#copy project
COPY ./src /var/www

WORKDIR /var/www



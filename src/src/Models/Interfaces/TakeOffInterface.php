<?php

namespace App\Models\Interfaces;

interface TakeOffInterface
{
    public function canTakeOff(): bool;
}
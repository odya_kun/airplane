<?php

namespace App\Models\Interfaces;

interface LandInterface
{
    public function canLand(): bool;
}
<?php

namespace App\Models\Abstracts;

use App\Models\Interfaces\FlyInterface;
use App\Models\Interfaces\LandInterface;
use App\Models\Interfaces\TakeOffInterface;

abstract class AbstractAirplaneModel implements FlyInterface, LandInterface, TakeOffInterface
{
    /**
     * @return array
     */
    abstract public function getFlyParams(): array;

    /**
     * @return array
     */
    abstract public function getLandParams(): array;

    /**
     * @return array
     */
    abstract public function getTakeOffParams(): array;

    /**
     * @param string $value
     * @return bool
     */
    public function canFlyOn(string $value): bool
    {
        if (true === $this->canFly()) {
            return in_array($value, $this->getFlyParams(), true);
        }

        return false;
    }

    /**
     * @param string $value
     * @return bool
     */
    public function canLandFrom(string $value): bool
    {
        if (true === $this->canLand()) {
            return in_array($value, $this->getLandParams(), true);
        }

        return false;
    }

    /**
     * @param string $value
     * @return bool
     */
    public function canTackOffFrom(string $value): bool
    {
        if (true === $this->canTakeOff()) {
            return in_array($value, $this->getTakeOffParams(), true);
        }

        return false;
    }
}
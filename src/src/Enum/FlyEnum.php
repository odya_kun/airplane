<?php

namespace App\Enum;

class FlyEnum
{
    public const ANY_TIME = 'any time';
    public const ANY_WEATHER = 'any weather';
    public const DAYTIME = 'daytime';
    public const GOOD_WEATHER = 'good weather';
}
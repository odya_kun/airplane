<?php

namespace App\Enum;

class TakeOffEnum
{
    public const RUNWAY = 'runway';
    public const WATER = 'water';
}
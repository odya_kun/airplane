<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191207162448 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE hangar (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE airplane_model (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE airplane (id INT AUTO_INCREMENT NOT NULL, hangar_id INT DEFAULT NULL, model_id INT DEFAULT NULL, serial_number VARCHAR(255) NOT NULL, INDEX IDX_2636002D2FEFB5A5 (hangar_id), INDEX IDX_2636002D7975B7E7 (model_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE airplane ADD CONSTRAINT FK_2636002D2FEFB5A5 FOREIGN KEY (hangar_id) REFERENCES hangar (id)');
        $this->addSql('ALTER TABLE airplane ADD CONSTRAINT FK_2636002D7975B7E7 FOREIGN KEY (model_id) REFERENCES airplane_model (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE airplane DROP FOREIGN KEY FK_2636002D2FEFB5A5');
        $this->addSql('ALTER TABLE airplane DROP FOREIGN KEY FK_2636002D7975B7E7');
        $this->addSql('DROP TABLE hangar');
        $this->addSql('DROP TABLE airplane_model');
        $this->addSql('DROP TABLE airplane');
    }
}

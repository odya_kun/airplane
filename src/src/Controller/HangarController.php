<?php

namespace App\Controller;

use App\Entity\AirplaneModel;
use App\Entity\Hangar;
use App\Repository\AirplaneModelRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HangarController extends AbstractController
{
    /**
     * @Route("/hangars/{id}", name="hangars_airplanes_list", requirements={"id"="\d+"}, methods={"GET"})
     * @param Hangar $hangar
     * @param AirplaneModelRepository $repository
     * @return JsonResponse
     */
    public function getModelsList(Hangar $hangar, AirplaneModelRepository $repository)
    {
        $data = array_map(
            function (AirplaneModel $airplaneModel) {
                return [
                    'model' => $airplaneModel->getName(),
                    'count' => $airplaneModel->getAirplanes()->count()
                ];
            },
            $repository->getAirplaneModelsByHangar($hangar)
        );

        return new JsonResponse($data, Response::HTTP_OK);
    }
}
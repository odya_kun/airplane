<?php

namespace App\Tests;

use App\Models\BoeingModel;
use PHPUnit\Framework\TestCase;

class AirplaneModelTest extends TestCase
{
    public function testCanBoeingFly()
    {
        $boeing = new BoeingModel();
        $this->assertTrue($boeing->canFly());
        $this->assertTrue($boeing->canFlyOn('any time'));
        $this->assertFalse($boeing->canFlyOn('daytime'));
    }
}
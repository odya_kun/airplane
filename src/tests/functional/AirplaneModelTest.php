<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiHangarListTest extends WebTestCase
{
    /**
     * @dataProvider hangarsListData
     * @param $id
     * @param $code
     */
    public function testGetList($id, $code, $count)
    {
        $client = static::createClient();

        $client->request('GET', sprintf('/hangars/%s', $id));

        $this->assertEquals($code, $client->getResponse()->getStatusCode());

        $response = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount($count, (array)$response);
    }

    public function hangarsListData()
    {
        return [
            [
                'id' => 1,
                'response_code' => 200,
                'count' => 3
            ],
            [
                'id' => 2,
                'response_code' => 404,
                'count' => 0
            ],
        ];
    }
}